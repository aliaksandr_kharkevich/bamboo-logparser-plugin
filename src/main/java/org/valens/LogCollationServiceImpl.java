package org.valens;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.build.test.TestCollationServiceImpl;
import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.build.test.TestReportProvider;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.utils.FileVisitor;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.google.common.collect.Sets;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class LogCollationServiceImpl implements TestCollationService
{
    private static final Logger log = Logger.getLogger(TestCollationServiceImpl.class);
    // ------------------------------------------------------------------------------------------------------- Constants
    private static final int COLLATE_TESTS_THREAD_POOL_SIZE = 20;
    // ------------------------------------------------------------------------------------------------- Type Properties
    // ---------------------------------------------------------------------------------------------------- Dependencies
    // ---------------------------------------------------------------------------------------------------- Constructors
    // ----------------------------------------------------------------------------------------------- Interface Methods


    public void collateTestResults(@NotNull final TaskContext taskContext, @NotNull final TestReportProvider testReportProvider, boolean arg3)
    {
        final CurrentBuildResult buildResult = taskContext.getBuildContext().getBuildResult();
        final Set<TestResults> failedTestResults = Sets.newHashSet();
        final Set<TestResults> successfulTestResults = Sets.newHashSet();
        final Set<TestResults> skippedTestResults = Sets.newHashSet();

        final TestCollectionResult testCollectionResult = testReportProvider.getTestCollectionResult();
        failedTestResults.addAll(testCollectionResult.getFailedTestResults());
        successfulTestResults.addAll(testCollectionResult.getSuccessfulTestResults());
        skippedTestResults.addAll(testCollectionResult.getSkippedTestResults());
        buildResult.appendTestResults(successfulTestResults, failedTestResults, skippedTestResults);
    }

    public void collateTestResults(@NotNull final TaskContext taskContext, @NotNull final String filePattern, @NotNull final TestReportCollector testReportCollector, boolean arg3)
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();
        final CurrentBuildResult buildResult = taskContext.getBuildContext().getBuildResult();
        final Set<TestResults> failedTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final Set<TestResults> successfulTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final Set<TestResults> skippedTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final Date taskStartDate = new Date(System.currentTimeMillis());

        final AtomicInteger numberOfFilesFound = new AtomicInteger(0);

        try
        {
            final File sourceDirectory = taskContext.getRootDirectory();

            if (sourceDirectory.exists())
            {
                buildLogger.addBuildLogEntry("Parsing test results...");
                log.debug(String.format("Parsing test results in %s, file pattern %s", sourceDirectory.getAbsolutePath(), filePattern));

                final ExecutorService executorService = Executors.newFixedThreadPool(COLLATE_TESTS_THREAD_POOL_SIZE);

                FileVisitor fileVisitor = new FileVisitor(sourceDirectory)
                {
                    @Override
                    public void visitFile(final File file)
                    {
                        final String fileName = file.getName();
                        buildLogger.addBuildLogEntry("visitFile " + fileName);
                        if (fileExtensionIsSupported(fileName, testReportCollector) && (taskStartDate != null && file.lastModified() > taskStartDate.getTime()))
                        {
                            log.debug(String.format("Submitting file %s to test report collector", file));

                            executorService.submit(new Runnable()
                            {
                                public void run()
                                {
                                    try
                                    {
                                        numberOfFilesFound.incrementAndGet();
                                        TestCollectionResult result = testReportCollector.collect(file);

                                        log.info(String.format("File %s parsing result: %d successful, %d failed, %d skipped",
                                                               file,
                                                               result.getSuccessfulTestResults().size(),
                                                               result.getFailedTestResults().size(),
                                                               result.getSkippedTestResults().size()));

                                        failedTestResults.addAll(result.getFailedTestResults());
                                        successfulTestResults.addAll(result.getSuccessfulTestResults());
                                        skippedTestResults.addAll(result.getSkippedTestResults());
                                    }
                                    catch (Throwable e)
                                    {
                                        // Don't bother logging the error if file contains testng
                                        if (!fileName.contains("testng"))
                                        {
                                            log.error(buildLogger.addErrorLogEntry("Failed to parse test result file \"" + fileName + "\""), e);
                                        }
                                        else
                                        {
                                            log.info("Unable to parse file '" + fileName + " but it appears to be a TestNG file. Ignoring exception: " + e.getMessage(), e);
                                        }
                                    }
                                }
                            });
                        }
                        else
                        {
                            if (log.isTraceEnabled())
                            {
                                if (!fileExtensionIsSupported(fileName, testReportCollector))
                                {
                                    log.trace(String.format("File %s has extension not supported by test report collector %s", file, testReportCollector.getClass()));
                                }
                                else if (taskStartDate == null)
                                {
                                    log.trace(String.format("File %s was ignored because taskStartDate is null", file));
                                }
                                else if (file.lastModified() <= taskStartDate.getTime())
                                {
                                    log.trace(String.format("File %s was ignored because is was modified (%s) before task started (%s)",
                                                            file, file.lastModified(), taskStartDate.getTime()));
                                }
                            }
                        }
                    }

                    private boolean fileExtensionIsSupported(final String filename, final TestReportCollector testReportCollector)
                    {
                        final Set<String> supportedExtensions = testReportCollector.getSupportedFileExtensions();

                        return supportedExtensions.isEmpty() || FilenameUtils.isExtension(filename, supportedExtensions);
                    }
                };
                buildLogger.addBuildLogEntry("filePattern " + filePattern);
                fileVisitor.visitFilesThatMatch(filePattern);

                executorService.shutdown();
                executorService.awaitTermination(60, TimeUnit.MINUTES);
            }
            buildLogger.addBuildLogEntry("getBuildErrors " + buildResult.getBuildErrors());
            buildLogger.addBuildLogEntry("numberOfFilesFound " + numberOfFilesFound.get());
            if (buildResult.getBuildErrors().isEmpty() && numberOfFilesFound.get() == 0)
            {
                buildResult.getBuildErrors().add("Could not find test result reports in the " + sourceDirectory + " directory.");
            }
        }
        catch (InterruptedException e)
        {
            log.error(buildLogger.addErrorLogEntry("Failed to parse test result files. Build was interrupted."));
        }

        buildResult.appendTestResults(successfulTestResults, failedTestResults, skippedTestResults);
    }

	@Override
	public void collateTestResults(TaskContext arg0, String arg1) {
		
		
	}

	@Override
	public void collateTestResults(TaskContext arg0, TestReportProvider arg1) {
		collateTestResults(arg0, arg1, true);
		
	}

	@Override
	public void collateTestResults(TaskContext arg0, String arg1, boolean arg2) {
		collateTestResults(arg0,arg1);
		
	}

	@Override
	public void collateTestResults(TaskContext arg0, String arg1,
			TestReportCollector arg2) {
		collateTestResults(arg0, arg1, arg2, true);
		
	}



    // -------------------------------------------------------------------------------------------------- Action Methods
    // -------------------------------------------------------------------------------------------------- Public Methods
    // -------------------------------------------------------------------------------------- Basic Accessors / Mutators
}
