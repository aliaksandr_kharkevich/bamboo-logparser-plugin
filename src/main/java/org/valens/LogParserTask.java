package org.valens;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;
import org.apache.tools.ant.types.PatternSet;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.interceptors.ErrorMemorisingInterceptor;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.utils.FileVisitor;
import com.atlassian.bamboo.v2.build.CurrentBuildResult;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class LogParserTask implements TaskType {
	private final TestCollationService testCollationService;

	public LogParserTask(TestCollationService testCollationService) {
		this.testCollationService = testCollationService;
	}

	public TaskResult execute(TaskContext taskContext) throws TaskException {

		final CurrentBuildResult currentBuildResult = taskContext
				.getBuildContext().getBuildResult();
		final Set<TestResults> failedTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final Set<TestResults> successfulTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final Set<TestResults> skippedTestResults = Collections.synchronizedSet(Sets.<TestResults>newHashSet());
        final CurrentBuildResult buildResult = taskContext.getBuildContext().getBuildResult();
		final BuildLogger buildLogger = taskContext.getBuildLogger();
		
		ErrorMemorisingInterceptor errorLines = new ErrorMemorisingInterceptor();
        taskContext.getBuildLogger().getInterceptorStack().add(errorLines);
                
		String errormask = taskContext.getConfigurationMap().get("errormask");
		String nonerrormask = taskContext.getConfigurationMap().get("nonerrormask");
		String linemask = taskContext.getConfigurationMap().get("linemask");
		String testFilePattern = taskContext.getConfigurationMap().get(
				"filemask");
		String linesplittermask = taskContext.getConfigurationMap().get("linesplittermask");
		Integer tokennumber = 0;
		try{
			tokennumber = Integer.parseInt(taskContext.getConfigurationMap().get(
				"tokennumber"));
		}catch(Exception e){
			tokennumber = 0;
		}
		buildLogger.addBuildLogEntry("[" + errormask + "]");
		buildLogger.addBuildLogEntry("[" + nonerrormask + "]");
		buildLogger.addBuildLogEntry("[" + linemask + "]");
		buildLogger.addBuildLogEntry("[" + testFilePattern + "]");
		buildLogger.addBuildLogEntry("[" + linesplittermask + "]");
		buildLogger.addBuildLogEntry("[" + tokennumber + "]");

		try {
			FileSet fileSet = new FileSet();
			fileSet.setDir(taskContext.getRootDirectory());

			PatternSet.NameEntry include = fileSet.createInclude();
			include.setName(testFilePattern);

			DirectoryScanner ds = fileSet.getDirectoryScanner(new Project());
			String[] srcFiles = ds.getIncludedFiles();

			buildLogger.addBuildLogEntry("Root directory: "	+ taskContext.getRootDirectory());
			
			LogParserCollector lg = new LogParserCollector(errormask, nonerrormask, linemask, linesplittermask, tokennumber);
			
			for(String s: srcFiles){
				File file = new File(taskContext.getRootDirectory() + "/" + s);
				TestCollectionResult result = lg.collect(file);

				buildLogger.addBuildLogEntry(String.format("File %s parsing result: %d successful, %d failed, %d skipped",
	                                   file,
	                                   result.getSuccessfulTestResults().size(),
	                                   result.getFailedTestResults().size(),
	                                   result.getSkippedTestResults().size()));

	            failedTestResults.addAll(result.getFailedTestResults());
	            successfulTestResults.addAll(result.getSuccessfulTestResults());
	           
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new TaskException("Failed to execute task", e);
		}
		
		buildResult.appendTestResults(successfulTestResults, failedTestResults, skippedTestResults);
		
		currentBuildResult.addBuildErrors(errorLines.getErrorStringList());

		return TaskResultBuilder.create(taskContext).checkTestFailures().build();
	}
}