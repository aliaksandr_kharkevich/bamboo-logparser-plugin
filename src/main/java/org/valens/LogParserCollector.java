package org.valens;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportCollector;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Files;

public class LogParserCollector implements TestReportCollector
{
	private String errormask = null;
	private String nonerrormask = null;
	private String linemask = null;
	private String linesplittermask = null;
	private Integer tokennumber = 0;
	final Logger log = Logger.getLogger(TestCollationService.class);
	
	private LogParserCollector()
	{
		
	}
	
	public LogParserCollector(String errormask, String nonerrormask, String linemask, String linesplittermask, Integer tokennumber) {
		super();
		this.errormask = errormask;
		this.nonerrormask = nonerrormask;
		this.linemask = linemask;
		this.linesplittermask = linesplittermask;
		this.tokennumber = tokennumber;
	}

	public TestCollectionResult collect(File file) throws Exception
    {
        TestCollectionResultBuilder builder = new TestCollectionResultBuilder();
        
        
        Collection<TestResults> successfulTestResults = Lists.newArrayList();
        Collection<TestResults> failingTestResults = Lists.newArrayList();
        
        List<String> lines = Files.readLines(file, Charset.forName("UTF-8"));
        
        int k = 0;
        TestResults testResults1 = new TestResults("Test Files", file.getName(), "1.0");
		testResults1.setState(TestState.SUCCESS);
        successfulTestResults.add(testResults1);
        String testname = "";		 
        for (String line : lines)
        {
        	
        	if(!line.matches(linemask))
        		continue;
        	
        	k++;
        	
            String suiteName = "Test Suite " + file.getAbsolutePath();
            
            try{
            	testname = line.split(linesplittermask)[tokennumber];
            }catch(Exception e){
            	testname = line;
            }
            String testName = file.getName() + " test number " + k + " ("+ testname +")";
            Double duration = 1.0;
            TestResults testResults = new TestResults(suiteName, testName, duration.toString());
            if (line.matches(errormask) && !line.matches(nonerrormask))
            {
            	 testResults.setState(TestState.FAILED);
            	 
                 failingTestResults.add(testResults);
            }
            else            	
                {
                	 testResults.setState(TestState.SUCCESS);
                	 successfulTestResults.add(testResults);
                }
        }
 
        return builder
            .addSuccessfulTestResults(successfulTestResults)
            .addFailedTestResults(failingTestResults)
            .build();
    }
 
    public Set<String> getSupportedFileExtensions()
    {
    	Set<String> s = Sets.newHashSet("log");
    	s.add("LOG");
    	
        return s; 
    }
}