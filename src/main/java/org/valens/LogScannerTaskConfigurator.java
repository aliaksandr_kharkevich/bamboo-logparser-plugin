package org.valens;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.opensymphony.xwork.TextProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

public class LogScannerTaskConfigurator extends AbstractTaskConfigurator
{
 
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put("errormask", params.getString("errormask"));
		config.put("nonerrormask", params.getString("nonerrormask"));
        config.put("filemask", params.getString("filemask"));
        config.put("linemask", params.getString("linemask"));
        config.put("extension", params.getString("extension"));
        config.put("linesplittermask", params.getString("linesplittermask"));
        config.put("tokennumber", params.getString("tokennumber"));            
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        context.put("errormask", "^fail:.*");
		context.put("nonerrormask", "^ok.*");
        context.put("filemask", "*.*");
        context.put("linemask", ".*");
        context.put("extension", "log");
        context.put("linesplittermask", ",");
        context.put("tokennumber", "2");        
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put("errormask", taskDefinition.getConfiguration().get("errormask"));
		context.put("nonerrormask", taskDefinition.getConfiguration().get("nonerrormask"));
        context.put("filemask", taskDefinition.getConfiguration().get("filemask"));
        context.put("linemask", taskDefinition.getConfiguration().get("linemask"));
        context.put("extension", taskDefinition.getConfiguration().get("extension"));
        context.put("linesplittermask", taskDefinition.getConfiguration().get("linesplittermask"));
        context.put("tokennumber", taskDefinition.getConfiguration().get("tokennumber"));        
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put("errormask", taskDefinition.getConfiguration().get("errormask"));
		context.put("nonerrormask", taskDefinition.getConfiguration().get("nonerrormask"));
        context.put("filemask", taskDefinition.getConfiguration().get("filemask"));
        context.put("linemask", taskDefinition.getConfiguration().get("linemask"));
        context.put("extension", taskDefinition.getConfiguration().get("extension"));
        context.put("linesplittermask", taskDefinition.getConfiguration().get("linesplittermask"));
        context.put("tokennumber", taskDefinition.getConfiguration().get("tokennumber"));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);        
    }


}